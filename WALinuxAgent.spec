%global with_legacy 0
%global dracut_modname 97walinuxagent

Name:           WALinuxAgent
Version:        2.9.1.1
Release:        9%{?dist}
Summary:        The Microsoft Azure Linux Agent

License:        Apache-2.0
URL:            https://github.com/Azure/%{name}
Source0:        https://github.com/Azure/%{name}/archive/v%{version}.tar.gz
Source1:        module-setup.sh

Patch1:         0001-waagent.service-set-ConditionVirtualization-microsof.patch
# For RHEL-35963 - [Azure][WALA] Consider to disable Log collector [rhel-10]
Patch2: wla-Disable-automatic-log-collector.patch
# For RHEL-40966 - [Azure][WALA][RHEL-10] Provisioning failed if no ifcfg-eth0
Patch3: wla-redhat-Use-NetworkManager-to-set-DHCP-hostnames-on-r.patch
# For RHEL-46713 - [Azure][RHEL-10][WALA] waagent -collect-logs doesn't work and the log is confusing
Patch4: wla-skip-cgorup-monitor-2939.patch
# For RHEL-68796 - Please add `mana` to 99-azure-unmanaged-devices.conf of Azure image
Patch5: wla-redhat-Add-a-udev-rule-to-avoid-managing-slave-NICs-.patch

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-distro
Requires:       %name-udev = %version-%release
%if 0%{?fedora}
Requires:       ntfsprogs
%endif
Requires:       openssh
Requires:       openssh-server
Requires:       openssl
Requires:       parted
Requires:       python3-pyasn1
Requires:       iptables

BuildRequires:   systemd
Requires(post):  systemd
Requires(preun): systemd
Requires(postun): systemd

%description
The Microsoft Azure Linux Agent supports the provisioning and running of Linux
VMs in the Microsoft Azure cloud. This package should be installed on Linux disk
images that are built to run in the Microsoft Azure environment.

%if 0%{?with_legacy}
%package legacy
Summary:        The Microsoft Azure Linux Agent (legacy)
Requires:       %name = %version-%release
Requires:       python2
Requires:       net-tools

%description legacy
The Microsoft Azure Linux Agent supporting old version of extensions.
%endif

%package udev
Summary:        Udev rules for Microsoft Azure

%description udev
Udev rules specific to Microsoft Azure Virtual Machines.

%prep
%setup -q
%autopatch -p1

%build
%py3_build

%install
%{__python3} setup.py install -O1 --skip-build --root %{buildroot} --lnx-distro redhat

mkdir -p -m 0700 %{buildroot}%{_sharedstatedir}/waagent
mkdir -p %{buildroot}%{_localstatedir}/log
touch %{buildroot}%{_localstatedir}/log/waagent.log

mkdir -p %{buildroot}%{_udevrulesdir}
mv %{buildroot}%{_sysconfdir}/udev/rules.d/*.rules %{buildroot}%{_udevrulesdir}/

rm -rf %{buildroot}/%{python3_sitelib}/tests
rm -rf %{buildroot}/%{python3_sitelib}/__main__.py
rm -rf %{buildroot}/%{python3_sitelib}/__pycache__/__main__*.py*

sed -i 's,#!/usr/bin/env python,#!/usr/bin/python3,' %{buildroot}%{_sbindir}/waagent
%if 0%{?with_legacy}
sed -i 's,#!/usr/bin/env python,#!/usr/bin/python2,' %{buildroot}%{_sbindir}/waagent2.0
%else
rm -f %{buildroot}%{_sbindir}/waagent2.0
%endif
sed -i 's,/usr/bin/python ,/usr/bin/python3 ,' %{buildroot}%{_unitdir}/waagent.service

mv %{buildroot}%{_sysconfdir}/logrotate.d/waagent.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/%{name}

install -m0755 -D -t %{buildroot}%{_prefix}/lib/dracut/modules.d/%{dracut_modname}/ %{SOURCE1}

%post
%systemd_post waagent.service

%preun
%systemd_preun waagent.service

%postun
%systemd_postun_with_restart waagent.service
rm -rf %{_unitdir}/waagent.service.d/

%files
%doc LICENSE.txt NOTICE README.md
%ghost %{_localstatedir}/log/waagent.log
%ghost %{_unitdir}/waagent-network-setup.service
%dir %attr(0700, root, root) %{_sharedstatedir}/waagent
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%{_sbindir}/waagent
%config(noreplace) %{_sysconfdir}/waagent.conf
%{_unitdir}/waagent.service
%{_unitdir}/azure.slice
%{_unitdir}/azure-vmextensions.slice
%{python3_sitelib}/azurelinuxagent
%{python3_sitelib}/*.egg-info

%files udev
%{_udevrulesdir}/*.rules
%{_prefix}/lib/dracut/modules.d/%{dracut_modname}/*.sh

%if 0%{?with_legacy}
%files legacy
%{_sbindir}/waagent2.0
%endif

%changelog
* Mon Jan 13 2025 Miroslav Rezanina <mrezanin@redhat.com> - 2.9.1.1-9
- wla-redhat-Add-a-udev-rule-to-avoid-managing-slave-NICs-.patch [RHEL-68796]
- Resolves: RHEL-68796
  (Please add `mana` to 99-azure-unmanaged-devices.conf of Azure image)

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 2.9.1.1-8
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Aug 05 2024 Miroslav Rezanina <mrezanin@redhat.com> - 2.9.1.1-7
- wla-skip-cgorup-monitor-2939.patch [RHEL-46713]
- Resolves: RHEL-46713
  ([Azure][RHEL-10][WALA] waagent -collect-logs doesn't work and the log is confusing)

* Thu Jul 11 2024 Miroslav Rezanina <mrezanin@redhat.com> - 2.9.1.1-6
- wla-redhat-Use-NetworkManager-to-set-DHCP-hostnames-on-r.patch [RHEL-40966]
- wla-redhat-Remove-all-waagent-unit-files-when-uninstalli.patch [RHEL-40966]
- wla-redhat-Mark-directories-properly-in-the-files-list.patch [RHEL-40966]
- wla-redhat-Remove-files-inside-WALA-services-directory.patch [RHEL-40966]
- Resolves: RHEL-40966
  ([Azure][WALA][RHEL-10] Provisioning failed if no ifcfg-eth0)

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 2.9.1.1-5
- Bump release for June 2024 mass rebuild

* Tue May 14 2024 Miroslav Rezanina <mrezanin@redhat.com> - 2.9.1.1-4
- wla-Disable-automatic-log-collector.patch [RHEL-35963]
- Resolves: RHEL-35963
  ([Azure][WALA] Consider to disable Log collector [rhel-10])

* Mon Jan 22 2024 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Fri Jan 19 2024 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Wed Oct 18 2023 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.9.1.1-1
- Update to 2.9.1.1 (#2232763)

* Wed Jul 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 2.9.0.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Tue Jun 13 2023 Python Maint <python-maint@redhat.com> - 2.9.0.4-3
- Rebuilt for Python 3.12

* Tue May 30 2023 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.9.0.4-2
- Switch to SPDX identifiers for the license field

* Mon Mar 13 2023 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.9.0.4-1
- Update to 2.9.0.4 (#2177333)

* Fri Jan 20 2023 Dusty Mabe <dusty@dustymabe.com> - 2.8.0.11-3
- Move module-setup.sh into git

* Wed Jan 18 2023 Fedora Release Engineering <releng@fedoraproject.org> - 2.8.0.11-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Mon Oct 31 2022 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.8.0.11-1
- Update to 2.8.0.11 (#2128547)

* Tue Oct 18 2022 Chris Patterson <cpatterson@microsoft.com> - 2.7.3.0-2
- Add ConditionVirtualization=|microsoft triggering condition

* Wed Aug 03 2022 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.7.3.0-1
- Update to 2.7.3.0 (#2110155)

* Wed Jul 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 2.7.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Thu Jun 30 2022 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.7.1.0-1
- Update to 2.7.1.0 (#2097244)

* Mon Jun 13 2022 Python Maint <python-maint@redhat.com> - 2.7.0.6-2
- Rebuilt for Python 3.11

* Fri Apr 22 2022 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.7.0.6-1
- Update to 2.7.0.6 (#2040980)

* Wed Jan 19 2022 Fedora Release Engineering <releng@fedoraproject.org> - 2.5.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Jan 03 2022 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.5.0.2-1
- Update to 2.5.0.2 (#2008699)

* Wed Jul 21 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2.3.1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Mon Jul 19 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.3.1.1-1
- Update to 2.3.1.1 (#1982512)
- Require iptables for setting up persistent firewall rules

* Tue Jun 15 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.3.0.2-1
- Update to 2.3.0.2 (#1971116)

* Fri Jun 04 2021 Python Maint <python-maint@redhat.com> - 2.2.54.2-2
- Rebuilt for Python 3.10

* Fri May 21 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.54.2-1
- Update to 2.2.54.2 (#1916966)

* Tue Mar 02 2021 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 2.2.52-6
- Rebuilt for updated systemd-rpm-macros
  See https://pagure.io/fesco/issue/2583.

* Fri Feb 19 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.52-5
- Require ntfsprogs on Fedora only 

* Tue Jan 26 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.52-4
- Fix distro resolution for RedHat

* Mon Jan 25 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.52-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Fri Jan 15 2021 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.52-2
- Add udev rules to initramfs (#1909287)

* Wed Dec 09 2020 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.52-1
- Update to 2.2.52 (#1849923)
- Add not yet upstream patches supporting Python3.9 changes

* Mon Jul 27 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.48.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jun 09 2020 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.48.1-1
- Update to 2.2.48.1 (#1641605)
- Split udev rules to a separate subpackage (#1748432)

* Tue May 26 2020 Miro Hrončok <mhroncok@redhat.com> - 2.2.46-2
- Rebuilt for Python 3.9

* Wed Apr 15 2020 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.46-1
- Update to 2.2.46

* Tue Jan 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.40-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Thu Oct 03 2019 Miro Hrončok <mhroncok@redhat.com> - 2.2.40-6
- Rebuilt for Python 3.8.0rc1 (#1748018)

* Wed Aug 21 2019 Miro Hrončok <mhroncok@redhat.com> - 2.2.40-5
- Rebuilt for Python 3.8

* Wed Aug 21 2019 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.40-4
- Disable Python2 dependent 'legacy' subpackage (#1741029)

* Mon Aug 19 2019 Miro Hrončok <mhroncok@redhat.com> - 2.2.40-3
- Rebuilt for Python 3.8

* Wed Jul 24 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.40-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Mon Jun 03 2019 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.40-1
- Update to 2.2.40
- Fix FTBFS in the preparation for Python3.8 (#1705219)

* Thu Mar 14 2019 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.38-1
- Update to 2.2.38 (CVE-2019-0804)

* Thu Mar 14 2019 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.37-1
- Update to 2.2.37

* Thu Jan 31 2019 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.32-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Thu Sep 20 2018 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.32-1
- Update to 2.2.32.2

* Thu Jul 12 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.2.25-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 2.2.25-3
- Rebuilt for Python 3.7

* Wed Apr 25 2018 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.25-2
- Move net-tools dependency to WALinuxAgent-legacy (#1106781)

* Mon Apr 16 2018 Vitaly Kuznetsov <vkuznets@redhat.com> - 2.2.25-1
- Update to 2.2.25
- Switch to Python3
- Legacy subpackage with waagent2.0 supporting old extensions

* Wed Feb 28 2018 Iryna Shcherbina <ishcherb@redhat.com> - 2.0.18-5
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.0.18-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2.0.18-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2.0.18-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sat Apr 02 2016 Scott K Logan <logans@cottsay.net> - 2.0.18-1
- Update to 2.0.18

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.0.14-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jul 02 2015 Scott K Logan <logans@cottsay.net> - 2.0.14-1
- Update to 2.0.14

* Tue Jun 16 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Jun 01 2015 Scott K Logan <logans@cottsay.net> - 2.0.13-1
- Update to 2.0.13

* Thu Apr 02 2015 Scott K Logan <logans@cottsay.net> - 2.0.12-1
- Update to 2.0.12-Oracle

* Sat Jan 10 2015 Scott K Logan <logans@cottsay.net> - 2.0.11-2
- Use systemd for rhel7
- Own logrotate.d
- Fix python2-devel dep

* Sat Dec 20 2014 Scott K Logan <logans@cottsay.net> - 2.0.11-1
- Initial package
